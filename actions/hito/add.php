<?php
/*
*AGREGAR UN HITO A LA BITACORA DEL USUARIO O RESPONDER FORMULARIO
*
*/

use DateTime;
$fecha = new DateTime();
 
$creadoPor = elgg_get_logged_in_user_guid();
$group = get_input('group');
$user = get_input('user');//alumno
$title = get_input('nombre');
$descripcion = get_input('descripcion');
$hito_guid = get_input('hito');
$accion= get_input('accion');

$respuesta= get_input('respuesta');






if($hito_guid){
    $hito = get_entity($hito_guid);
    //RESPONDER FORMULARIO(alumno)
    if(($accion == 'ver' || $accion == "responder") && $user == elgg_get_logged_in_user_guid()){
        //se comprueba si usuario respondio el formulario     
        if($respuesta){//si se está editando la respuesta
            $respuesta = get_entity($respuesta);
        }else{
            
            $respuesta = new ElggObject();
            $respuesta->subtype = "respuesta";
            $respuesta->owner = elgg_get_logged_in_user_guid();
            $respuesta->access_id = 2 ;
            $respuesta->hito = $hito->guid;//hito al que pertenece una pregunta     
               
        }



        $preguntas = elgg_get_entities([ //Preguntas que corresponden a un hito
            'type' => 'Object',
            'subtype' => 'preguntaHito',
            'relationship' => 'pertenece a',
            'relationship_guid' => (int)$hito_guid,
            'inverse_relationship' => true, 
            
        ]);
        
        foreach($preguntas as $preguntas){           
            $valorInput = get_input('pregunta_'.$preguntas->guid);
            $tipoPregunta= $preguntas->tipoPregunta;
            //PARA PREGUNTAS DE TIPO TEXTO, SE GUARDA LA RESPUESTA EN UN METADATO COON EL NOMBRE: "PREGUNTA_{GUID-PREGUNTA}"
            $metadata = "pregunta_".$preguntas->guid;
            if($preguntas->tipoPregunta == "texto"){
                $respuesta->$metadata= $valorInput;        
                
                          
            } 
            
            if($preguntas->tipoPregunta == "archivo"){
                $file_type=$_FILES['pregunta_'.$preguntas->guid]['type'];
                $uploaded_files = elgg_get_uploaded_files('pregunta_'.$preguntas->guid);
                $uploaded_file = array_shift($uploaded_files);
               
                if($uploaded_file){     
                    $file = new ElggFile();
                    $file->owner_guid = elgg_get_logged_in_user_guid();
                    $file->subtype='file';
                    $file->access_id =2;
                    $file->hito= $hito->guid;
                    $file->pregunta= $preguntas->guid;
                
                    if ($file->acceptUploadedFile($uploaded_file)) {
                     $file->save(); 
                      //
                      $respuesta->$metadata =$file->guid;
                      //add_entity_relationship($archivo, 'pertenece a',$hito->guid);//archivo pertenece a hito    
                
                    }
                    // if(!$archivo){
                    //    register_error("archivo no subido!");
                    //    forward(REFERER);
                    // }
                } 


            }
           
        }
        $res = $respuesta->save();
        system_message("hito:guardado");
        // if(!$respuesta){
        //     add_entity_relationship($res, 'pertenece a',$hito->guid);//respuesta pertenece a hito
        // }        

        forward($sitio."hito/add/".$group."/".$user."/".$hito->guid ."/ver");
        
        
    }
    else{
        register_error("hito:NoPuedeResponder");
    }
}else{

    $hito = new ElggObject();
    $hito->subtype = 'hito';
    $hito->CreadoPor = $creadoPor;
    $hito->fecha =  $fecha->format("d M , Y");
    $hito->user = $user; //alumno
    $hito->group = $group;
    $hito->access_id = 2 ;
    $hito->comments_on = 'On';
}
if($accion == 'editar' || $accion == ""){
    $hito->title = $title;
    $hito->descripcion = $descripcion;
    $hito->estado = "habilitado";

    $guid = $hito->save();
}


if(!$hito_guid){
    add_entity_relationship($guid, 'pertenece a',$user);//hito pertenece a alumno
}


if ($guid) {
    system_message("hito:guardado");
    forward($sitio."hito/add/".$group."/".$user."/".$hito->guid ."/editar");
        
}else {
    // register_error("hito:ErrorGuardar");
    // forward(REFERER); // REFERER es una variable global que define la página anterior
}






