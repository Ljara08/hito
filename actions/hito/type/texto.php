<?php
/*
* AGREGA AL FORMULARIO UNA PREGUNTA DE TEXTO
*/
    
$hito = (int)get_input('hito');
$nombre = get_input('nombre');
$texto = get_input('texto');
$puntuacion = get_input('puntuacion');
$ReGeneral = get_input('ReGeneral');
$pregunta_guid = get_input('pregunta');


if($pregunta_guid){
    $pregunta = get_entity($pregunta_guid);
}else{
    $pregunta = new ElggObject();
    $pregunta->subtype = 'preguntaHito';
    $pregunta->tipoPregunta = 'texto';
    $pregunta->hito = $hito;
    $pregunta->owner= elgg_get_logged_in_user_guid();
    $pregunta->access_id = 2 ;
    $pregunta->respuesta = "";
}
$pregunta->title = $nombre;
$pregunta->texto = $texto;
$pregunta->puntuacion = $puntuacion;
$pregunta->ReGeneral = $ReGeneral;


$guid =$pregunta->save();

if(!$pregunta_guid){
    add_entity_relationship($guid, 'pertenece a',$hito);//pregunta pertenece a hito
}

if ($guid) {
    system_message("hito:guardado");
} else {
    register_error('hito:ErrorGuardar');
    forward(REFERER); // REFERER es una variable global que define la página anterior
}

