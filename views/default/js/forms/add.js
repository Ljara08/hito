//MODAL
var modal1 = document.getElementById("modal1");
var modal2 = document.getElementById("modal2");

var btn1 = document.getElementById("btn_modal1");
var btn2 = document.getElementById("btn_modal2");

var span1 = document.getElementsByClassName("close")[0];
var span2 = document.getElementsByClassName("close")[1];

btn1.onclick = function() {
    modal1.style.display = "block";
}

span1.onclick = function() {
    modal1.style.display = "none";
}
span2.onclick = function() {
    modal2.style.display = "none";
}

window.onclick = function(event) {
    if (event.target == modal1) {
        modal1.style.display = "none";
    }
    if (event.target == modal2) {
        modal2.style.display = "none";
    }
}

//SELECCIONAR TIPO DE PREGUNTAS EN MODAL
//OPCIONES DE PREGUNTAS EN MODAL
var desc={
    'pregunta':[
        {  'nombre':'Verdadero / Falso', 'descripcion': 'Pregunta de opción múltiple con solamente dos opciones: Falso o Verdadadero.'    },
        {  'nombre':'Seleccion multiple', 'descripcion': 'Permite seleccionar una o varias respuestas de una lista pre-definida.'    },
        {  'nombre':'texto', 'descripcion': 'Permite ingresar texto como respuesta.'  },
        {  'nombre':'Archivo', 'descripcion': 'Permite subir un archivo.'  },
    ]     
};


$( "li" ).each(function( index ) {
    $(this).click(function(){
        if($( this ).text() =='Verdadero / Falso'){
            $("#descripcion").text(desc.pregunta[0].descripcion);
        }
        if($( this ).text()=='Selección Multiple'){
            $("#descripcion").text(desc.pregunta[1].descripcion);
        }
        if($( this ).text()=='Texto'){
            $("#descripcion").text(desc.pregunta[2].descripcion);
        }
        if($( this ).text()=='Archivo'){
            $("#descripcion").text(desc.pregunta[3].descripcion);
        }
    });
});

//FUNCION PARA SELECCIONAR QUE TIPO DE PREGUNTA SE AGREGA AL FORMULARIO y redireccionar pagina
var radioValue;
$("input[type='radio']").click(function(){
    radioValue = $("input[name='TipoPregunta']:checked").val();   
});
function ElegirTipoPregunta(sitio,hito,pregunta=null,tipoPregunta=null){
    modal1.style.display = "none";
    modal2.style.display = "block";
    if(radioValue && tipoPregunta==null){
        tipoPregunta=radioValue;
    }
    require(['elgg/Ajax'], Ajax => {
        var ajax = new Ajax();
        ajax.form('hito/type/'+tipoPregunta+'',{
            data:{
                hito:hito,
                pregunta:pregunta,
            }
        }).done(function (output, statusText, jqXHR) {
            if (jqXHR.AjaxData.status == -1) {
                return;
            } 
            $(".addQuestion").html(output);
                        
        });          
    }); 
}

