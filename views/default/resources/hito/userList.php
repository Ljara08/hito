<?php
elgg_gatekeeper();
$group=elgg_extract("group", $vars);
$group= get_entity($group);

elgg_entity_gatekeeper($group->guid, 'group');


$title= elgg_echo('hito:userList');
$nombreGrupo = elgg_get_friendly_title($group->name);  
$content = elgg_view("output/url", array(
    "href" => elgg_get_site_url()."groups/profile/".$group->guid."/".$nombreGrupo,
    "text" => elgg_echo('hito:volverGrupo'), 
    "class" => "elgg-button elgg-button-submit center", 
    "is_trusted" => true
));


$content.=elgg_view_title($title);
$content.=elgg_list_entities([
    'subtype'=> 'user',
    'relationship' => 'member',
    'relationship_guid' =>$group->guid,
    'inverse_relationship' => true,  
    'item_view' => 'items/itemUser',  
    'group'     =>  $group->guid,

]);

$body = elgg_view_layout('one_sidebar', array(
    'content' => $content,
    'sidebar' => $sidebar
));

echo elgg_view_page("Cuestionario", $body);
