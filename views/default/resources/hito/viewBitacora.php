<?php
/*
*MUESTRA BITACORA DE USUARIO
*/
$usuario = elgg_extract("user", $vars);
$usuario = get_entity($usuario);//alumno
$grupo = elgg_extract("group", $vars);
$grupo = get_entity($grupo);
$title = elgg_echo('hito:BitacoraDe');
$agregar = elgg_echo('hito:agregar:hito');
$plus = elgg_view_icon('plus');
$sitio = elgg_get_site_url();
$editIcon = elgg_view_icon('edit');
$verIcon = elgg_view_icon('eye');
$eliminarIcon = elgg_view_icon('delete');
$usuarioLogeado = elgg_get_logged_in_user_guid();
$usuarioLogeado = get_entity($usuarioLogeado);



$content = elgg_view("output/url", array(
    "href" => elgg_get_site_url()."hito/users/".$grupo->guid."/",
    "text" => elgg_echo('hito:volver'), 
    "class" => "elgg-button elgg-button-submit center", 
    "is_trusted" => true
));


$hitos = elgg_get_entities([ //hitos del usuario
    'type' => 'Object',
    'subtype' => 'hito',
    'relationship' => 'pertenece a',
    'relationship_guid' => $usuario->guid,
    'inverse_relationship' => true, 
]);
$contentHito="";
foreach($hitos as $hito){

    $CreadoEn = $hito->group;
    $CreadoEn = get_entity($CreadoEn);  
    $eliminarHito=elgg_view('output/url', [
		'href' => elgg_generate_action_url('entity/delete', [
			'guid' => $hito->guid,
		]),
		'text' => $eliminarIcon,		
		'confirm' => true,
	]);
    $editar = elgg_view('output/url', array(
        'text' => $editIcon,
        'href' => "hito/add/$grupo->guid/$usuario->guid/$hito->guid/editar",
        'is_action' => false,
    ));
    $ver = elgg_view('output/url', array(
        'text' => $verIcon,
        'href' => "hito/add/$grupo->guid/$usuario->guid/$hito->guid/ver",
        'is_action' => false,
    ));
    
    if($hito->CreadoPor == $usuarioLogeado->guid ){
        $acciones = <<<___HTML
            <div class=accionesHito > $eliminarHito </div>
            <div class=accionesHito > $editar </div>

        ___HTML;

    }
    $vars['entity'] =$hito;
	$params =  $vars;
	
    $comentar= elgg_view('object/elements/summary', $params);
    $comentar = elgg_view_menu('social', [
        'entity' => elgg_extract('entity', $vars),
        'handler' => elgg_extract('handler', $vars),
        'class' => 'elgg-menu-hz',
    ]);


    $contentHito .= <<<___HTML
        <li class'content'>
            <div class="content">
                <h3>
                    $hito->title 
                    $acciones
                    <div class=accionesHito > $ver </div>                    
                </h3>
                $hito->descripcion
                $CreadoEn->name
                $comentar
            </div>
            <div class="point"></div>
            <div class="date">
                <h4> $hito->fecha </h4>
                
            </div>
        </li >
    ___HTML;
    //--------------------------------
    // $params = [
	// 	// 'content' => "fff",
	// 	//'icon' => true,
	// ];
    // $vars['entity'] =$hito;
	// $params =  $vars;
	// $contentHito.= elgg_view('object/elements/summary', $params);
	
	
    //................................
}

if($grupo instanceof ElggGroup){
    $agregarHito = <<<___HTML
        <i onclick="agregarHito($usuario->guid,$grupo->guid,'$sitio')" title=$agregar>$plus</i>
    ___HTML;
}

$timeline = <<<___HTML
<body class'content'>
    <div class="container">
        <h1> $title $usuario->name $agregarHito</h1>       
        <br>
        <br>
        <div class="timeline">
            <ul class'content'>
                $contentHito
            </ul>
        </div>      
    </div>   
</body>
___HTML;



$content.=$timeline;


$body = elgg_view_layout('one_sidebar', array(
    'content' => $content,
    'sidebar' => $sidebar
));

echo elgg_view_page("Nuevo Hito", $body);

echo <<<___HTML
    <script type='text/javascript' src='{$sitio}mod/hito/views/default/js/resources/viewBitacora.js'></script>  
___HTML;
?>
<script>
 
   
</script>