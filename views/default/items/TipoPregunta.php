<?php
/*
 *LISTA PERSONALIZADA DE PREGUNTAS QUE CORRESPONDEN A UN HITO 
*/
$pregunta = elgg_extract('entity', $vars);
$hito = elgg_extract("hito", $vars);
$sitio=elgg_get_site_url();
$accion = elgg_extract("accion", $vars);
$tipoPregunta = $pregunta->tipoPregunta;


$pre = elgg_extract('f', $vars);

echo $pregunta;
$tipoPregunta = $pregunta->tipoPregunta;
$texto = $pregunta->texto;
#MOSTRAR ICONOS DE EDICION Y ELIMINA SOLO SI SE ESTÁ EDITANDO EL FORMULARIUO
if($accion == 'editar'){
    $iconEditar = elgg_view_icon('edit');
    $iconEliminar = elgg_view_icon('delete');

    $eliminarPregunta=elgg_view('output/url', [
        'href' => elgg_generate_action_url('entity/delete', [
            'guid' => $pregunta->guid,
        ]),
        'text' => $iconEliminar,
        'confirm' => true,
    ]);
    
    $editarPregunta = elgg_view('output/url', array(
        'text' => $iconEditar,
        'is_action' => false,
    ));

}

$form_vars = array('enctype' => 'multipart/form-data');
$form = elgg_view_form("hito/viewTypes/".$tipoPregunta,$form_vars, $vars);
if($accion=='editar'){
    $form="";
}

echo <<<___HTML
    
            <div class="cont">
            <h3 class='tittle'> $pregunta->texto </h3> 
            <div onclick="ElegirTipoPregunta('$sitio',$hito,$pregunta->guid,'$tipoPregunta')">$iconEditar</div>                         
            $eliminarPregunta 
              
            </div>
            $form 
            
    
    <br>
___HTML;
