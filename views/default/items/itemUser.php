<?php
/*
*MUESTRA ENTIDADES "USUARIO" DE UN GRUPO, VISTA PERSONALIZADA PARA VER BITACORA
*/

$entity=elgg_extract('entity', $vars);
$group=elgg_extract('group', $vars);
$sitio=elgg_get_site_url();

$icon = elgg_view_entity_icon($entity, 'small');
$Bitacora=elgg_echo('hito:ver');

echo  <<<___HTML
		<div class='elgg-image-block clearfix elgg-river-item'>
			<div class='elgg-image'>
				<div class='elgg-avatar  '>
					$icon				
				</div>
                <div class='elgg-avatar  '>
					<h3>$entity->name </h3>
				</div>
			</div>
			<div class='elgg-body'>
                <div class="elgg-listing-summary-metadata">
                    <nav class="elgg-menu-container elgg-menu-entity-container" data-menu-name="entity">
                        <ul class='elgg-menu elgg-menu-social elgg-menu-hz elgg-menu-social-default'>
                            <li class='elgg-menu-item-comment  '>     
                                <a href="{$sitio}hito/viewBitacora/$group/$entity->guid" class="elgg-anchor"> $Bitacora </a>                          
                            </li>
                            
                        </ul>					
                    </nav>
                </div>
				
			</div>
		</div>
		
___HTML;
