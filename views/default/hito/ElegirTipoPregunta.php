<?php

$group = elgg_extract('group', $vars);
$group = get_entity($group);
$user = elgg_extract('user', $vars);
$hito = elgg_extract('hito', $vars);
$sitio=elgg_get_site_url();


$titulo = elgg_echo('hito:elegirPregunta:titulo');

echo " <p>$titulo</p> ";



echo elgg_view_field([
    '#type' => 'hidden',
    'name' => 'group',
    'value' =>  $group->guid,
    
]);


$preguntas=elgg_view_field([
    '#type' => 'radio',
    'name' => 'TipoPregunta',
    'id' => 'TipoPregunta',
    'options' => array(elgg_echo('hito:VerdaderFalso')=>'Verdadero/Falso', elgg_echo('hito:SeleccionMultiple') =>'SeleccionMultiple', elgg_echo('hito:texto') =>'texto',elgg_echo('hito:archivo')=> 'archivo'),
    '#class'=>'elgg-table'
    
]);

$html=<<<___HTML
<div class="grids">
    <div class="div2">
        $preguntas
    </div>
    <div class="div3 1fr" id="descripcion">
        Seleccione una pregunta para ver su descripción.
    </div>
    
   
    

</div>
    <!-- <div> <a  href="#">  <blockquote  class="center"> </blockquote>  </a> </div> -->

___HTML;
echo $html;
$btnOk= elgg_echo('hito:agregarTipoPregunta');
echo  <<<___HTML

    <div id="elegirTipoPregunta" onclick="ElegirTipoPregunta('$sitio',$hito)"><a href="#ex2" rel="modal:open">$btnOk</a></div>
___HTML;
