<?php
echo elgg_echo('hito:TipoPregunta').": ";
echo "<br><br>";
$pregunta = elgg_extract("pregunta", $vars);
$pregunta = get_entity($pregunta);
$hito = elgg_extract('hito', $vars);
$icon=elgg_view_icon('angle-up');



echo elgg_view_field([
    '#type' => 'hidden',
    'name' => 'hito',
    'value' => $hito
]);
echo elgg_view_field([
    '#type' => 'hidden',
    'name' => 'pregunta',
    'value' => $pregunta->guid
]);

//----------- GENERAL--------------------------

$nombrePregunta= elgg_view_field([
    '#type' => 'text',
   // '#label'=>elgg_echo('Nombre de la pregunta'),
    'name' => 'nombre',
    'required'=> 'true', 
    'value'  =>$pregunta->title,
]);
$textoPregunta= elgg_view_field([
    '#type' => 'longtext',
  //  '#label'=>elgg_echo('Texto de la pregunta'),
    'name' => 'texto',
    'required'=> 'true', 
    'value'  =>$pregunta->texto,
]);
$puntuacion= elgg_view_field([
    '#type' => 'number',
   // '#label'=>elgg_echo('Puntuación'),
    'name' => 'puntuacion',
    'value'=> $pregunta->puntuacion,
    //'required'=> 'true',   
]);
$ReGeneral= elgg_view_field([
    '#type' => 'longtext',
    //'#label'=>elgg_echo('Retroalimentación General'),
    'name' => 'ReGeneral',
    'value'=> $pregunta->ReGeneral,
    //'required'=> 'true',   
]);

$labelNombrePregunta = elgg_echo('hito:NombrePregunta');
$labelTextoPregunta = elgg_echo('hito:TextoPregunta');
$labelPuntuacion = elgg_echo('hito:Puntuacion');
$labelRetroalimentacionGeneral = elgg_echo('hito:RetroalimentacionGeneral');

echo <<<___HTML
<!-- GENERAL-->
    <fieldset>
       
        <!--  ------------------------------------------------- -->
        <div class=''>       
           
            <!--  ------------------------------------------------- -->    
            <div class="labelContainer">
                $labelNombrePregunta
            </div>
            <div class="inputContainer">
                $nombrePregunta      
            </div>    
            <!--  ------------------------------------------------- -->      
            <div class="labelContainer">
                $labelTextoPregunta
            </div>
            <div class="inputContainer">
                $textoPregunta      
            </div>    
            <!--  ------------------------------------------------- -->  
            <div class="labelContainer">
                $labelPuntuacion
            </div>
            <div class="inputContainer">
                $puntuacion      
            </div>    
            <!--  ------------------------------------------------- -->      
            <div class="labelContainer">
            $labelRetroalimentacionGeneral
            </div>
            <div class="inputContainer">
                $ReGeneral      
            </div>    
            <!--  ------------------------------------------------- -->                       
               

        </div>

    </fieldset>    
    <br> 
___HTML;



//--FIN GENERAL-----

echo elgg_view_field(array(
    '#type' => 'submit',
    'name' => 'guardarPregunta',
    'id' => 'guardarPregunta',
    '#class' => 'center',
    'value' => elgg_echo('hito:guardar' ),
 ));

// elgg_set_form_footer($submit); 


?>