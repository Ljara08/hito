<?php
$group = elgg_extract("group", $vars);
$group = get_entity($group);
$user = elgg_extract("user", $vars);
$user = get_entity($user);
$hito = elgg_extract("hito", $vars);
$hito = get_entity($hito);
$accion = elgg_extract("accion", $vars);
$sitio=elgg_get_site_url();

//se comprueba si usuario respondio el formulario
$respuesta= elgg_get_entities([
    'type' => 'Object',
    'subtype' => 'respuesta',
    'relationship' => 'pertenece a',
    'relationship_guid' => $hito->guid,
   'inverse_relationship' => true, 
]);

echo elgg_view("output/url", array(
    "href" => elgg_get_site_url()."hito/viewBitacora/".$group->guid."/".$user->guid,
    "text" => elgg_echo('hito:volver'), 
    "class" => "elgg-button elgg-button-submit center", 
    "is_trusted" => true
));



//INPUTs
echo elgg_view_field([
    '#type' => 'hidden',
    'name' => 'group',
    'value' => $group->guid
]);
echo elgg_view_field([
    '#type' => 'hidden',
    'name' => 'user',
    'value' => $user->guid,//alumno
]);
echo elgg_view_field([
    '#type' => 'hidden',
    'name' => 'hito',
    'value' => $hito->guid,
]);
echo elgg_view_field([
    '#type' => 'hidden',
    'name' => 'accion',
    'value' => $accion,
]);

echo elgg_view_field([
    '#type' => 'hidden',
    'name' => 'respuesta',
    'value' => $respuesta[0]->guid,
]);
$mostrarPreguntas="";
if($hito && ($accion == 'ver' || $accion == 'responder')){//si está en modo 'ver' o "responder"

    echo <<<___HTML
        <h1>$hito->title</h1>
        $hito->descripcion
    ___HTML;

    $preguntas= elgg_get_entities([ //Preguntas que corresponden a un hito
        'type' => 'Object',
        'subtype' => 'preguntaHito',
        'relationship' => 'pertenece a',
        'relationship_guid' => $hito->guid,
        'inverse_relationship' => true, 

    ]);
    

    foreach($preguntas as $pregunta){  
        $mostrarArchivo='';      
        //$vars['entity'] = get_entity($pregunta->guid);
        $metadata ="pregunta_$pregunta->guid";//si ya hay una repuesta se obtiene el nombre del metadato que contiene la respuesta, de tipo texto
        if($pregunta->tipoPregunta == "texto"){
            
            $inputPregunta = elgg_view_field([
                '#type' => 'longtext',
                'name' => "pregunta_$pregunta->guid",
                'value' => $respuesta[0]->$metadata,
            ]);
        }
        if($pregunta->tipoPregunta == "archivo"){
          
            $inputPregunta =  elgg_view_field([
                '#type' => 'file',
            //  '#label' => $file_label,
                'name' => "pregunta_$pregunta->guid",
                // 'value' => ($guid),
                'required' => false
            ]);
            $file= get_entity($respuesta[0]->$metadata);
            $file = elgg_view_entity($file, [
                'full_view' => false,
                'show_responses' => true,
            ]);

            $mostrarArchivo= <<<___HTML
            <div>$file<div>
            ___HTML;



         
        }
    
        $mostrarPreguntas.=<<<___HTML

                    <div class="cont">
                    <h3 class='tittle'> $pregunta->texto</h3>                         
                    </div>
                    $inputPregunta
                    $mostrarArchivo
        ___HTML;
     
        
    }
    echo $mostrarPreguntas;


}
//----------------------------------------------------------------------
if(!$hito || $accion == 'editar' ){ //si está en modo 'edicion' de hito o crear
    $plus = elgg_view_icon('plus');

    echo elgg_view_field([
        '#type' => 'text',
        'name' => 'nombre',
        '#label' => elgg_echo('hito:input:nombre'),
        'required' => true,
        'value' => $hito->title
    ]);
    //ID_Pagina
    echo elgg_view_field([
        '#type' => 'longtext',
        'name' => 'descripcion',
        '#label' => elgg_echo('hito:input:descripcion'),
        'value' => $hito->descripcion
    ]);
    
    #MODAL PARA GREGAR UNA PREGUNTA AL FORMULARIO
    $add = elgg_view('hito/ElegirTipoPregunta',$vars);
    $agregar = elgg_echo('hito:agregarPregunta');


    if($hito){

        $preguntass= elgg_get_entities([ //Preguntas que corresponden a un hito
            'type' => 'Object',
            'subtype' => 'preguntaHito',
            'relationship' => 'pertenece a',
            'relationship_guid' => $hito->guid,
            'inverse_relationship' => true, 
           // 'item_view' => 'items/TipoPregunta',
           // 'accion' => $accion
        ]);
        
        foreach($preguntass as $p){
            $vars['entity'] = get_entity($p->guid);        
            echo elgg_view('items/TipoPregunta',$vars);
            
        }

        
        
        echo <<<___HTML
            <div id="btn_modal1"><a href="#ex1" rel="modal:open" title=$agregar>$plus</a></div>
            <div class"elgg-page elgg-page-default">
                <!-- Modal HTML embedded directly into document -->
                <div name="ex1" id="modal1" class="modal1">
                    <div class='modal-content'>
                    <span class="close">&times;</span>
                        $add
                    </div>
                    
                </div>
            </div>
        ___HTML;
        echo <<<___HTML
            
            <div class"elgg-page elgg-page-default">
                <div name="ex2" id="modal2" class="modal1">
                    <div class='modal-content'>
                    <span class="close">&times;</span>
                    <div class='addQuestion'></div>
                    </div>
                    
                </div>
            </div>
        ___HTML;
    }
      
   

}


echo "<br><br>";

    
// build form footer
echo elgg_view_field([
    '#type' => 'submit',
    'value' => elgg_echo('save'),
    '#class' => 'center',
    'confirm' => true,
]);





?>
