<?php

function hito_init() {
    

    elgg_extend_view("css/elgg", "css/hito/style");
    elgg_extend_view("css/elgg", "css/hito/timeline");
    elgg_extend_view("css/elgg", "css/hito/modal");
    elgg_extend_view("css/elgg", "css/hito/formularioPreguntas");

    elgg_register_ajax_view('forms/hito/type/texto');
    elgg_register_ajax_view('forms/hito/type/archivo');

    elgg_register_action("hito/add", __DIR__. "/actions/hito/add.php");
    elgg_register_action("hito/type/texto", __DIR__. "/actions/hito/type/texto.php");
    elgg_register_action("hito/type/archivo", __DIR__. "/actions/hito/type/archivo.php");

    elgg_register_page_handler('hito', 'hito_page_handler');

    elgg_register_plugin_hook_handler('entity:url', 'object', 'hito_set_url');
    elgg_register_plugin_hook_handler('register', 'menu:owner_block', 'hito_owner_block_menu');


};

function hito_page_handler($segments) {
    switch ($segments[0]) {
        case 'add':     
            $resource_vars['group'] = elgg_extract(1, $segments);	//id grupo     
            $resource_vars['user'] = elgg_extract(2, $segments);	//id usuario 
            $resource_vars['hito'] = elgg_extract(3, $segments);
            $resource_vars['accion'] = elgg_extract(4, $segments);	//accion
            echo elgg_view_resource('hito/add',$resource_vars);
        break;
        case 'viewBitacora':
            $resource_vars['group'] = elgg_extract(1, $segments);	//id grupo
            $resource_vars['user'] = elgg_extract(2, $segments);	//id usuario  
            echo elgg_view_resource('hito/viewBitacora', $resource_vars);
        break;
        case 'view':
            $resource_vars['group'] = elgg_extract(1, $segments);	//id grupo
            $resource_vars['user'] = elgg_extract(2, $segments);	//id usuario  
            echo elgg_view_resource('hito/view', $resource_vars);
        break;
        case 'users':
            $resource_vars['group'] = elgg_extract(1, $segments);	//id grupo   
            echo elgg_view_resource('hito/userList', $resource_vars);  
        break;
      
            

    }

    return true;
};


function hito_set_url($hook, $type, $url, $params) {
    $entity = $params['entity'];
    $group= $entity->group;
    if ($entity->getSubtype() === 'hito') {
        return "hito/view/{$group}/{$entity->guid}";
    }
};

function hito_owner_block_menu($hook, $type, $return, $params){
	$entity = elgg_extract('entity', $params);
 
	$group = elgg_get_page_owner_entity(); //grupo
	$owner_group = $group->owner_guid;//propietario grupo
	$user = elgg_get_logged_in_user_guid(); //usuario actual
    $user = get_entity($user);

    if($entity instanceof ElggGroup ) {$link = "hito/users/{$entity->guid}";}
    if($entity instanceof ElggUser ) {$link = "hito/viewBitacora/{$entity->guid}/{$entity->guid}";}
    
    $url = $link ;
    $text =elgg_echo('hito:menu:alumnos'	);
    $item = new ElggMenuItem('menuHito', $text,$url);
    if($entity instanceof ElggGroup && ($user->rol == "profesor" || $user->rol == 'supervisor' || $user->guid == $owner_group)){    
       
        $return[] = $item;
    }   
    if($entity instanceof ElggUser && ($user->rol == "profesor" || $user->rol == 'supervisor' || $entity->guid == $user->guid)){ 
 
        $return[] = $item;
    }                    
   
   
	return $return;
}




return function() {
    // register an initializer
    elgg_register_event_handler('init', 'system', 'hito_init');
};