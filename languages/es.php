<?php
/**
 * Translation file
 *
 * Note: don't change the return array to short notation because Transifex can't handle those during `tx push -s`
 */

return array(

    
	'hito:input:nombre'         	=> 'Nombre',
	'hito:input:descripcion'    	=> 'Descripción',
	'hito:input:texto'          	=> 'Ingrese texto',
    'hito:add'                  	=> 'Agregar nuevo Hito',
	'hito:add-View'					=> 'Hito',
	'hito:guardado'					=> 'Guardado',
	'hito:menu:alumnos'				=> 'Bitacora de usuario',
	'hito:userList'					=> 'Usuarios',
	'hito:ver'						=> 'Ver bitacora',
	'hito:BitacoraDe'				=> 'Bitacora de',
	'hito:agregar:hito'				=> 'Agregar Tarea',
	'hito:agregarPregunta'			=> 'Agregar una  nueva pregunta al formulario',
	'hito:elegirPregunta:titulo'	=> 'Elija un tipo de pregunta para agregar al formulario',
	'hito:VerdaderFalso'			=> 'Verdadero / Falso',
	'hito:SeleccionMultiple'		=> 'Selección Multiple',
	'hito:texto'					=> 'Texto',
	'hito:archivo'					=> 'Archivo',
	'hito:agregarTipoPregunta'		=> 'Ok',
	'hito:TipoPregunta'				=> 'Tipo de Pregunta',
	'hito:guardar'					=> 'Guardar',
	'hito:ErrorGuardar'				=> 'Error al Guardar',
	'hito:NombrePregunta'			=> 'Nombre de la Pregunta',
	'hito:TextoPregunta'			=> 'Texto de la Pregunta',
	'hito:Puntuacion'				=> 'Puntuación',
	'hito:RetroalimentacionGeneral'	=> 'Retroalimentación General',
	'hito:DescripcionArchivo'		=> 'Ingrese un texto',
	'hito:NoPuedeResponder'			=>'No puede responder este cuestionario',
	'hito:volver'					=> 'Volver',
	'hito:volverGrupo'				=> 'Volver al grupo',
);